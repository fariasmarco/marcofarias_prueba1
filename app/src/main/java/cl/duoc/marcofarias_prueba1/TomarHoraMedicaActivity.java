package cl.duoc.marcofarias_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class TomarHoraMedicaActivity extends AppCompatActivity {

    private EditText etNombrePaciente, etFecha, etHora, etTelefono;
    private Button btnGuardarHora, btnlimpiar, btnVerListado, btnCerrarSesion;
    private ArrayList<Paciente> datos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tomar_hora_medica);
        initView();
    }

    private void initView(){
        etNombrePaciente = (EditText)findViewById(R.id.etNombrePaciente);
        etFecha = (EditText)findViewById(R.id.etFecha);
        etHora = (EditText)findViewById(R.id.etHora);
        etTelefono = (EditText)findViewById(R.id.etTelefono);
        btnCerrarSesion = (Button)findViewById(R.id.btnCerrarSesion);
        btnlimpiar = (Button)findViewById(R.id.btnLimpiar);
        btnVerListado = (Button)findViewById(R.id.btnVerListado);


        datos = new ArrayList<>();
        btnGuardarHora = (Button)findViewById(R.id.btnGuardarHora);
        btnGuardarHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(AdministrarPaciente.getInstance().guardarPaciente(obtenerPaciente())){
                    Toast.makeText(TomarHoraMedicaActivity.this,"Hora Tomada Correctamente", Toast.LENGTH_SHORT).show();
                    limpiarCampos();
                }else{
                    Toast.makeText(TomarHoraMedicaActivity.this, "La Hora ya fue reservada", Toast.LENGTH_SHORT).show();
                }
            }
        });


        btnCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cerrarSesion = new Intent(TomarHoraMedicaActivity.this, LoginPacienteActivity.class);
                startActivity(cerrarSesion);
            }
        });

        btnVerListado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent verListado = new Intent(TomarHoraMedicaActivity.this, ListadoDeHorasTomadasActivity.class);
                startActivity(verListado);
            }
        });

        btnlimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiarCampos();
            }
        });
    }

    private void limpiarCampos() {
        etNombrePaciente.setText("");
        etFecha.setText("");
        etHora.setText("");
        etTelefono.setText("");
        etNombrePaciente.requestFocus();
    }

    private Paciente obtenerPaciente() {
        return new Paciente(getTextET(etNombrePaciente), getTextET(etHora), getTextET(etFecha), getTextET(etHora));
    }

    private String getTextET(EditText et) {
        return et.getText().toString();
    }

}
