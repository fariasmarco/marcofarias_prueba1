package cl.duoc.marcofarias_prueba1;

/**
 * Created by Marco on 23-09-17.
 */

public class Paciente {
    private String nombrePaciente;
    private String fecha;
    private String hora;
    private String telefono;

    public Paciente(String nombrePaciente, String fecha, String hora, String telefono){
        this.nombrePaciente = nombrePaciente;
        this.fecha = fecha;
        this.hora = hora;
        this.telefono = telefono;
    }


    public String getNombrePaciente() {
        return nombrePaciente;
    }

    public void setNombrePaciente(String nombrePaciente) {
        this.nombrePaciente = nombrePaciente;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public boolean equals(Object p) {
        if (p instanceof Paciente){
            Paciente aux =(Paciente)p;
            return aux.getNombrePaciente().equals(this.getNombrePaciente())
                    && aux.getFecha().equals(this.getFecha())
                    && aux.getHora().equals(this.getHora())
                    && aux.getTelefono().equals(this.getTelefono());
        }
        return false;
    }
}
