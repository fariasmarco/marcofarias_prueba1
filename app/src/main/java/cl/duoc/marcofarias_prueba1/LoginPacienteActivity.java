package cl.duoc.marcofarias_prueba1;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginPacienteActivity extends AppCompatActivity {

    private EditText etUsuario, etClave;
    private Button btnEntrar, btnCallCenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_paciente);

        etUsuario = (EditText)findViewById(R.id.etUsuario);
        etClave = (EditText)findViewById(R.id.etClave);
        btnEntrar = (Button)findViewById(R.id.btnEntrar);
        btnCallCenter = (Button)findViewById(R.id.btnCallCenter);

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validarUsuario();
            }
        });

        //btnCallCenter.setOnClickListener(new View.OnClickListener() {
        //    @Override
        //    public void onClick(View view) {
        //        Intent call = new Intent(Intent.ACTION_CALL);
        //        call.setData(Uri.parse("tel:+5695555555"));
        //        startActivity(call);
        //    }
        //});

    }

    private void validarUsuario(){
        if(etUsuario.getText().toString().equals("admin") && etClave.getText().toString().equals("admin") ||
                etUsuario.getText().toString().equals("root") && etClave.getText().toString().equals("root") ||
                etUsuario.getText().toString().equals("morty") && etClave.getText().toString().equals("morty")) {
            Intent entrar = new Intent(this, TomarHoraMedicaActivity.class);
            startActivity(entrar);
            finish();
        }else {
            Toast.makeText(this, "Usuario y/o Clave Invalidos", Toast.LENGTH_SHORT).show();
        }
    }

}
