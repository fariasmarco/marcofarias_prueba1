package cl.duoc.marcofarias_prueba1;

import java.util.ArrayList;

/**
 * Created by Marco on 23-09-17.
 */

public class AdministrarPaciente {

    private static AdministrarPaciente instance;
    static {
        instance = new AdministrarPaciente();
        for (int x = 0; x < 500; x++) {
            instance.guardarPaciente(new Paciente("n"+x, "f"+x, "h"+x, "t"+x));
        }
    }
    private ArrayList<Paciente> values;

    protected AdministrarPaciente() {
        values = new ArrayList<>();
    }

    public static AdministrarPaciente getInstance() {
        if (instance == null) {
            instance = new AdministrarPaciente();
        }
        return instance;
    }

    public boolean guardarPaciente (Paciente paciente) {
        if(!buscarPaciente(paciente)) {
            values.add(paciente);
            return true;
        }else {
            return false;
        }
    }

    public boolean buscarPaciente(Paciente paciente) {
        for (Paciente aux : values) {
            if (paciente.equals(aux)) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<Paciente> listarPacientes(){
        return values;
    }

}
