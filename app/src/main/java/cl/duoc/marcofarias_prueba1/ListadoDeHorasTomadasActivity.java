package cl.duoc.marcofarias_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ListViewCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class ListadoDeHorasTomadasActivity extends AppCompatActivity {

    private ListView lvListarHorasMedicas;
    private ListadoDeHorasTomadasAdapter listadoDeHorasTomadasAdapter;
    private Button btnVolver;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_de_horas_tomadas);

        btnVolver = (Button)findViewById(R.id.btnVolver);

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent volver = new Intent(ListadoDeHorasTomadasActivity.this, TomarHoraMedicaActivity.class);
                startActivity(volver);
            }
        });



        lvListarHorasMedicas = (ListView)findViewById(R.id.lvListarPacientes);
        listadoDeHorasTomadasAdapter = new ListadoDeHorasTomadasAdapter(AdministrarPaciente.getInstance().listarPacientes(),this);

        lvListarHorasMedicas.setAdapter(listadoDeHorasTomadasAdapter);
    }




}
