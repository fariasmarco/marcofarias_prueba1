package cl.duoc.marcofarias_prueba1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by Marco on 23-09-17.
 */

public class ListadoDeHorasTomadasAdapter extends BaseAdapter {

    private ArrayList<Paciente> values;
    private Context context;

    public ListadoDeHorasTomadasAdapter(ArrayList<Paciente> values, Context context) {
        this.values = values;
        this.context = context;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int position) {
        return values.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            convertView = inflater.inflate(R.layout.item_paciente, parent, false);

            holder = new ViewHolder();
            holder.tvNombrePaciente = (TextView) convertView.findViewById(R.id.tvNombrePaciente);
            holder.tvHoraTomada = (TextView) convertView.findViewById(R.id.tvHoraTomada);
            holder.tvFechaTomada = (TextView) convertView.findViewById(R.id.tvFechaTomada);
            holder.tvTelefono = (TextView) convertView.findViewById(R.id.tvTelefono);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Paciente aux = values.get(position);
        holder.tvNombrePaciente.setText(aux.getNombrePaciente());
        holder.tvHoraTomada.setText(aux.getHora());
        holder.tvFechaTomada.setText(aux.getFecha());
        holder.tvTelefono.setText(aux.getTelefono());

        return convertView;
    }

    private static class ViewHolder {
        TextView tvNombrePaciente;
        TextView tvHoraTomada;
        TextView tvFechaTomada;
        TextView tvTelefono;
    }

}
